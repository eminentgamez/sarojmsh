<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- HTML5 Shiv -->
        <!--[if lt IE 9]>
                <script src="<?php echo get_template_directory_uri(); ?>/layout/js/plugins/html5shiv/html5.js"></script>
        <![endif]-->
      <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    <?php do_action( 'mtl_before_header' ); ?>
    <header id="header" style="background: #17A6E0;">
        <div class="container" >
            <div class="row" >
                <div class="col-lg-4 col-sm-12"></div>
                <div class="col-lg-8 col-sm-12">
                    <div id="logo">
                        <?php
                        $img_logo = get_theme_mod( 'regina_lite_img_logo' );
                        $text_logo = get_theme_mod( 'regina_lite_text_logo', __( 'Mahesh Multispeciality Hospital', 'regina-lite' ) );
                        ?>
                        <a href="<?php echo esc_url( get_site_url() ); ?>" title="<?php echo esc_attr( $text_logo ); ?>">
                            <?php if( $img_logo ): ?>
                                <img src="<?php echo esc_url( $img_logo ); ?>" alt="<?php esc_attr( get_bloginfo( 'name' ) ); ?>" title="<?php esc_attr( get_bloginfo( 'name' ) ); ?>" />
                            <?php else: ?>
                                <span class="logo-title"><?php echo esc_html( $text_logo ); ?></span>
                            <?php endif; ?>
                        </a>
                    </div><!--#logo-->
                </div>
                <div class="col-lg-6 col-sm-12" style="line-height: 5px;"></div>
                <div class="col-lg-6 col-sm-12" style="line-height: 14px;">
                       <span class="logo-address">, Mangalore - 20</span>
                </div>
            </div>
            <div class="row" style="max-height: 20px;" >
                <div class="col-md-3 col-sm-12">
                    <button class="mobile-nav-btn hidden-lg"><span class="nc-icon-glyph ui-2_menu-bold"></span></button>
                </div><!--.col-lg-3-->
                <div class="col-md-12 col-sm-12">
                    <nav id="navigation">
                        <ul class="main-menu">
                            <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'primary',
                                'menu'              => '',
                                'container'         => '',
                                'container_class'   => '',
                                'container_id'      => '',
                                'menu_class'        => '',
                                'menu_id'           => '',
                                'items_wrap'        => '%3$s',
                                'walker'            => new MTL_Extended_Menu_Walker(),
                                'fallback_cb'       => 'MTL_Extended_Menu_Walker::fallback'
                            ) );
                            ?>
                            <?php $bookappointmenturl = get_theme_mod( 'regina_lite_contact_bar_bookappointmenturl', 'regina-lite' ); ?>
                            <?php if( $bookappointmenturl ){  ?>
                                <li class="hide-mobile"><a href="<?php echo esc_url( $bookappointmenturl ); ?>" title="<?php _e( 'Book Appointment', 'regina-lite' ); ?>"><span class="nc-icon-glyph ui-1_bell-53"></span> <?php _e( 'Book Appointment', 'regina-lite' ); ?></a></li>
                            <?php }  ?>
                            <?php $enable_site_search_icon = get_theme_mod( 'regina_lite_enable_site_search_icon', 1 ); ?>
                            <?php if( $enable_site_search_icon == 1 ) { ?>
                                <li class="hide-mobile"><a href="#" title="<?php _e( 'Search', 'regina-lite' ); ?>" class="nav-search"><span class="nc-icon-outline ui-1_zoom"></span></a></li>
                            <?php } ?>
                        </ul>
                        <div class="nav-search-box hidden-lg">
                            <input type="text" placeholder="<?php _e( 'Search', 'regina-lite' ); ?>">
                            <button class="search-btn"><span class="nc-icon-outline ui-1_zoom"></span></button>
                        </div>
                    </nav><!--#navigation-->
                </div><!--.col-lg-9-->
            </div><!--.row-->
        </div><!--.container-->
    </header><!--#header-->

    <?php
    global $wp_customize;
    $preloader_enabled = get_theme_mod('regina_lite_enable_site_preloader', 1);

    if( !isset( $wp_customize ) && $preloader_enabled == 1 ) { ?>

    <!-- Site Preloader -->
    <div id="page-loader">
        <div class="page-loader-inner">
            <div class="loader"></div>
        </div>
    </div>
    <!-- END Site Preloader -->
    <?php } ?>
