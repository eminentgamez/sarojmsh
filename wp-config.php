<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V*q,5,4HMQ]ahH*/#;T(o|!=iLMXWw/ge }QtfJCk>1mohS2T_(<[3a>3|G|@Ua+');
define('SECURE_AUTH_KEY',  'ACZ%ZNEkTP%po^aj+$9>EQxRRN$9Y.~IWh|oHT!VisJ7Z+*f#KB/2q}Z|/|bjwg[');
define('LOGGED_IN_KEY',    'p:RNcF|*rUJ%Q&Q}[TE#{6,n7m7dt]/iSS_[7cp0NDtFI]~JM&),T~MaWR ;}V;n');
define('NONCE_KEY',        '0&huIW9.UMC`rJ/2kwI/4:zz%3Eec^o+`_CU/qHCY#(Q3>fY)S/J<8bOkF7SXr` ');
define('AUTH_SALT',        '3x^Iwg=]`02D90MZ)#3J6fGY3jspG{suoyR,nNi+?j;I:]~3u;^{.ys|g1I+|M`[');
define('SECURE_AUTH_SALT', 'W^|NWVsnhC4Z_HaxH3`m-]7KqpcCH5c!Q#n|Ct&:+O% >(.WrV~x!HO.CPyu<gp%');
define('LOGGED_IN_SALT',   '8Rcj%(yH~ZwRHp;W[sWtC|kCgc7~i82r0V!AsaB8KyG =zZ7mevCn(W!Jv0L_+XR');
define('NONCE_SALT',       '}Trg!jF)I3b4m_@|~MzY}r|r}en#rs;U?#Atv-y/Q3#Ef4*l-@bz&Q{^us7El~89');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
